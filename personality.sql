-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2016 at 10:56 AM
-- Server version: 5.7.10
-- PHP Version: 7.0.2-4+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `personality`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `trait1` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `trait2` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `scoring_key` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `operation` tinyint(4) NOT NULL,
  `answer` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `trait1`, `trait2`, `scoring_key`, `operation`, `answer`) VALUES
(1, 'makes lists', 'relies on memory', 'JP', 1, NULL),
(2, 'sceptical', 'wants to believe', 'FT', -1, NULL),
(3, 'bored by time alone', 'needs time alone', 'IE', -1, NULL),
(4, 'accepts things as they are', 'unsatisfied with the way things are', 'SN', 1, NULL),
(5, 'keeps a clean room', 'just puts stuff where ever', 'JP', 1, NULL),
(6, 'thinks "robotic" is an insult', 'strives to have a mechanical mind', 'FT', 1, NULL),
(7, 'energetic', 'mellow', 'IE', -1, NULL),
(8, 'prefers to take multiple choice test', 'prefers essay answers', 'SN', 1, NULL),
(9, 'chaotic', 'organized', 'JP', -1, NULL),
(10, 'easily hurt', 'thick-skinned', 'FT', 1, NULL),
(11, 'works best in groups', 'works best alone', 'IE', -1, NULL),
(12, 'focused on the present', 'focused on the future', 'SN', 1, NULL),
(13, 'plans far ahead', 'plans at the last minute', 'JP', 1, NULL),
(14, 'wants people\\\'s respect', 'wants their love', 'FT', -1, NULL),
(15, 'gets worn out by parties', 'gets fired up by parties', 'IE', 1, NULL),
(16, 'fits in', 'stands out', 'SN', 1, NULL),
(17, 'keeps options open', 'commits', 'JP', -1, NULL),
(18, 'wants to be good at fixing things', 'wants to be good at fixing people', 'FT', -1, NULL),
(19, 'talks more', 'listens more', 'IE', -1, NULL),
(20, 'when describing an event, will tell what happened', 'when descriving an event, will tell what it meant', 'SN', 1, NULL),
(21, 'gets work done right away', 'procrastinates', 'JP', 1, NULL),
(22, 'follows the heart', 'follows the head', 'FT', 1, NULL),
(23, 'stays at home', 'goes out on the town', 'IE', 1, NULL),
(24, 'wants the big picture', 'wants the details', 'SN', -1, NULL),
(25, 'improvises', 'prepares', 'JP', -1, NULL),
(26, 'bases morality on justice', 'bases morality on compassion', 'FT', -1, NULL),
(27, 'finds it difficult to yell loudly', 'yelling at others when they are far away comes naturally', 'IE', 1, NULL),
(28, 'theoretical', 'empirical', 'SN', -1, NULL),
(29, 'works hard', 'plays hard', 'JP', 1, NULL),
(30, 'uncomfortable with emotions', 'values emotions', 'FT', -1, NULL),
(31, 'likes to perform in front of other people', 'avoids public speaking', 'IE', -1, NULL),
(32, 'likes to know "who?", "what?", "when?"', 'likes to know "why?"', 'SN', 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
