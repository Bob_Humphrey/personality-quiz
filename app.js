var question = {
  trait1: 'makes lists',
  trait2: 'relies on memory',
  scoringKey: 'JP',
  operation: 1
}

var questions = [
  {
    trait1: 'makes lists',
    trait2: 'relies on memory',
    scoringKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'sceptical',
    trait2: 'wants to believe',
    scoringKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'bored by time alone',
    trait2: 'needs time alone',
    scoringKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'accepts things as they are',
    trait2: 'unsatisfied with the way things are',
    scoringKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'keeps a clean room',
    trait2: 'just puts stuff where ever',
    scoringKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'thinks "robotic" is an insult',
    trait2: 'strives to have a mechanical mind',
    scoringKey: 'FT',
    operation: 1,
    answer: null
  },
  {
    trait1: 'energetic',
    trait2: 'mellow',
    scoringKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'prefers to take multiple choice test',
    trait2: 'prefers essay answers',
    scoringKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'chaotic',
    trait2: 'organized',
    scoringKey: 'JP',
    operation: -1,
    answer: null
  },
  {
    trait1: 'easily hurt',
    trait2: 'thick-skinned',
    scoringKey: 'FT',
    operation: 1,
    answer: null
  },
  {
    trait1: 'works best in groups',
    trait2: 'works best alone',
    scoringKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'focused on the present',
    trait2: 'focused on the future',
    scoringKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'plans far ahead',
    trait2: 'plans at the last minute',
    scoringKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'wants people\'s respect',
    trait2: 'wants their love',
    scoringKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'gets worn out by parties',
    trait2: 'gets fired up by parties',
    scoringKey: 'IE',
    operation: 1,
    answer: null
  },
  {
    trait1: 'fits in',
    trait2: 'stands out',
    scoringKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'keeps options open',
    trait2: 'commits',
    scoringKey: 'JP',
    operation: -1,
    answer: null
  },
  {
    trait1: 'wants to be good at fixing things',
    trait2: 'wants to be good at fixing people',
    scoringKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'talks more',
    trait2: 'listens more',
    scoringKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'when describing an event, will tell what happened',
    trait2: 'when describing an event, will tell what it meant',
    scoringKey: 'SN',
    operation: 1,
    answer: null
  },
  {
    trait1: 'gets work done right away',
    trait2: 'procrastinates',
    scoringKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'follows the heart',
    trait2: 'follows the head',
    scoringKey: 'FT',
    operation: 1,
    answer: null
  },
  {
    trait1: 'stays at home',
    trait2: 'goes out on the town',
    scoringKey: 'IE',
    operation: 1,
    answer: null
  },
  {
    trait1: 'wants the big picture',
    trait2: 'wants the details',
    scoringKey: 'SN',
    operation: -1,
    answer: null
  },
  {
    trait1: 'improvises',
    trait2: 'prepares',
    scoringKey: 'JP',
    operation: -1,
    answer: null
  },
  {
    trait1: 'bases morality on justice',
    trait2: 'bases morality on compassion',
    scoringKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'finds it difficult to yell loudly',
    trait2: 'yelling to others when they are far away comes naturally',
    scoringKey: 'IE',
    operation: 1,
    answer: null
  },
  {
    trait1: 'theoretical',
    trait2: 'empirical',
    scoringKey: 'SN',
    operation: -1,
    answer: null
  },
  {
    trait1: 'works hard',
    trait2: 'plays hard',
    scoringKey: 'JP',
    operation: 1,
    answer: null
  },
  {
    trait1: 'uncomfortable with emotions',
    trait2: 'values emotions',
    scoringKey: 'FT',
    operation: -1,
    answer: null
  },
  {
    trait1: 'likes to perform in front of other people',
    trait2: 'avoids public speaking',
    scoringKey: 'IE',
    operation: -1,
    answer: null
  },
  {
    trait1: 'likes to know "who?", "what?", "when?"',
    trait2: 'likes to know "why?"',
    scoringKey: 'SN',
    operation: 1,
    answer: null
  }
]

var scoringKeys = ['IE', 'SN', 'FT', 'JP'];

var scoringKeyCounter = {
    IE: 0,
    SN: 0,
    FT: 0,
    JP: 0
}

var scores = {
    IE: 0,
    SN: 0,
    FT: 0,
    JP: 0
}

var results = {
  IE: 30,
  SN: 12,
  FT: 30,
  JP: 18
}

var averages = {
  IE: [30, 27, 24, 21, 24, 21, 24, 27, 24],
  SN: [12, 15, 18, 21, 24, 27, 24, 21, 24],
  FT: [30, 27, 30, 33, 30, 27, 30, 27, 24],
  JP: [18, 21, 24, 21, 24, 21, 24, 21, 24]
}

var chartData = {
  IE: [
    {
      value: 12,
      color: "#0D47A1",
    },
    {
      value: 12,
      color:"#039BE5",
    },
  ],
  SN: [
    {
      value: 12,
      color: "#0D47A1",
    },
    {
      value: 12,
      color:"#039BE5",
    },
  ],
  FT: [
    {
      value: 12,
      color: "#0D47A1",
    },
    {
      value: 12,
      color:"#039BE5",
    },
  ],
  JP: [
    {
      value: 12,
      color: "#0D47A1",
    },
    {
      value: 12,
      color:"#039BE5",
    },
  ],
}

new Vue({
  el: 'body',
  data: {
    question,
    questionNumber: 1,
    answers: [],
    results,
    scores,
  },

  ready:  function() {
    for (index = 0; index < scoringKeys.length; ++index) {
      console.log(scoringKeys[index]);
      var scoringKey = scoringKeys[index];
      var chartId = scoringKey.toLowerCase() + 'Chart';
      var ctx = document.getElementById(chartId).getContext("2d");
      Chart.defaults.global.animation = false;
      Chart.defaults.global.showTooltips = false;
      var chart = new Chart(ctx).Doughnut(chartData[scoringKey]);
    }
  },





  methods: {
    handleAnswer: function (answer) {
      // Save the user's answer to the questions array.
      questions[this.questionNumber - 1].answer = answer;
      // Get the scoringKey and operation for the current question.
      var scoringKey = questions[this.questionNumber - 1].scoringKey;
      var operation = questions[this.questionNumber - 1].operation;
      // Keep track of how many questions have been answered for each scoringKey.
      scoringKeyCounter[scoringKey] = scoringKeyCounter[scoringKey] + 1;
      // Update the cumulative results for the current question scoring key.
      this.results[scoringKey] = this.results[scoringKey] + (answer * operation);
      // How many questions have been answered for the current question scoring key.
      var counter = scoringKeyCounter[scoringKey];
      // The average score for the current question scoring key based on how many
      // questions have been answered so far for that scoring key.
      var average = averages[scoringKey][counter];
      // The current score for the current question scoring key.
      var score = this.results[scoringKey] - average;
      // Update the scores array for the current question scoring key.
      this.scores[scoringKey] = score;
      // Charts
      var chartId = scoringKey.toLowerCase() + 'Chart';
      var rightValue = (score - ( counter * (-2))) * (1 / (counter * 4));
      console.log("rightValue=" + rightValue);
      var leftValue = 1 - rightValue;
      console.log("leftValue=" + leftValue);
      chartData[scoringKey][1].value = rightValue;
      chartData[scoringKey][0].value = leftValue;
      var ctx = document.getElementById(chartId).getContext("2d");
      Chart.defaults.global.animation = false;
      Chart.defaults.global.showTooltips = false;
      var chart = new Chart(ctx).Doughnut(chartData[scoringKey]);
      // Save the user's answer to the current question.
      this.answers.push(answer);
      // Get the next question.
      this.questionNumber++;
      this.question = questions[this.questionNumber - 1];
      // Remove the previously selected button focus.
      document.activeElement.blur();
    },
    enableButton: function () {
      this.buttonDisabled = false;
    },
  }
})
